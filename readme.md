# digitalocean K8s-cluster  

Cluster created via Digitalocean K8S service and configuration managed via
ansible tasks.

the cluster use traefik as Ingress controller using LB from Digitalocean service, and deploy grafana,prometheus,nginx(demo) app in a automated way via ansible tasks.

DNS is created or deleted from an ansible task via DO API.


###### Requeriments
```
Helm v2.14.1
Terraform 0.12.3
ansible 2.8.0
```
####terraform backend

using Digitalocean spaces object storage (s3 compatible) as backend for the .tfstate of terraform

init
```
terraform init 

```
init with tfvars 
```
terraform init --backend-config example.tfvars 
```

[digitalocean spaces api-keys](https://www.digitalocean.com/community/tutorials/how-to-create-a-digitalocean-space-and-api-key)

example.tfvars file content
```
#bucket backend
bucket="bucketname"
access_key=""
secret_key=""
``` 

#### var used

some VARS are necessary to create the env between terraform and ansible

export that variables to start using it.

[digitalocean-api-token](https://www.digitalocean.com/docs/api/create-personal-access-token/)

```
TF_VAR_do_cluster_name=""
TF_VAR_domain=""
TF_VAR_do_token="" 
```
or export vars via .sh
example
```
source ./terraform-vars.sh
```

## creating cluster terraform 

if you only want to create a digital ocean k8s cluster you only have to invoque terraform in the terraform folder.

plan
```
terraform plan
```
create
```
terraform apply
```
destroy
```
terraform destroy
```
##### note:
dont forget to destroy inused resources to avoid unnecessary charges.

## install.sh

already create an install.sh with args to install or remove the cluster in a more useful way.


```
┌🤘-🐧jrab66@ 💻 jrab66 🤖 []: - 🧱 k8s-digitalocean on 🌵  master •1 ✗
└🤘-> ./install.sh -h
create k8s cluster (terraform/ansible)
args: -i Install , -d Delete
Usage: install.sh -i terraform
Usage: install.sh -i ansible
Usage: install.sh -d all
Usage: install.sh -d terraform
Usage: install.sh -d ansible
Usage: install.sh -d all
```


## bonus 
### stress testing
already added a stress testing chart to test grafana/prometheus.


inside the helm folder execute 
install
```
helm install load-gen --name load-gen --set replicaCount=6 load-gen-master
```

remove
```
helm delete --purge load-gen
```
resource "digitalocean_kubernetes_cluster" "k8s-cluster" {
  name    = var.do_cluster_name
  region  = "nyc1"
  version = "1.14.1-do.4"

  node_pool {
    name       = "worker-pool"
    size       = "s-2vcpu-4gb"
    node_count = 3
  }

##install
#   provisioner "local-exec" {
#     command = "sleep 300"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/do-config.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/helm.yml"
#   }
#   provisioner "local-exec" {
#     command = "sleep 100"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/traefik.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/monitoring.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/todoapp.yml"
#   }
#   provisioner "local-exec" {
#     command = "sleep 100"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/dnsrecords.yml"
#   }

# ## remove
#   provisioner "local-exec" {
#     when    = "destroy"
#     command = "ansible-playbook ../ansible/tasks/do-config.yml"
#   }
#   provisioner "local-exec" {
#     when    = "destroy"
#     command = "ansible-playbook ../ansible/tasks/deletednsrecords.yml"
#   }
#   provisioner "local-exec" {
#     when    = "destroy"
#     command = "ansible-playbook ../ansible/tasks/removemonitoring.yml"
#   }

}

# resource "null_resource" "ansiblework" {
#   triggers = {
#     static_trigger = var.digitalocean_kubernetes_cluster.k8s-cluster.id
#  }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/do-config.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/helm.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/traefik.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/monitoring.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/todoapp.yml"
#   }
#   provisioner "local-exec" {
#     command = "ansible-playbook ../ansible/tasks/dnsrecords.yml"
#   }
  
# }


# resource "null_resource" "ansiblework-delete" {
#   provisioner "local-exec" {
#     when    = "destroy"
#     command = "ansible-playbook ../ansible/tasks/deletednsrecords.yml"
#   }
#   provisioner "local-exec" {
#     when    = "destroy"
#     command = "ansible-playbook ../ansible/tasks/removemonitoring.yml"
#   }
#   triggers = {
#     "before" = var.digitalocean_kubernetes_cluster.k8s-cluster.id
#  }
# }


# output "cluster-id" {
#   value = digitalocean_kubernetes_cluster.my_digital_ocean_cluster.id
# }

output "domain" {
  value = var.domain
}


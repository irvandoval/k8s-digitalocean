terraform project

create k8s cluster in DO
create ansible playbook to install helm and shoot 


steps
install helm
install traefik and create loadbalancer for app
add persistence claim
install prometheous and grafana via helm

# version software

```
Helm v2.14.1
Terraform 0.12.3
ansible 2.8.0
# Terraform

terraform init
```
terraform init

terraform init --backend-config example.tfvars 
```


terraform plan
```
terraform apply -var-file=digitalocean.tfvars
```


grab config k8s cluster
```
curl -X GET \
-H "Content-Type: application/json" \
-H "Authorization: Bearer ${TF_VAR_do_token}" \
"https://api.digitalocean.com/v2/kubernetes/clusters/$CLUSTER_ID/kubeconfig" \
> config
```
check k8s cluster
```
kubectl --kubeconfig=config  get nodes
```

expose terraform var
```
terraform output  cluster-id
```

revision of var 
```
curl -X GET \
-H "Content-Type: application/json" \
-H "Authorization: Bearer ${TF_VAR_do_token}" \
"https://api.digitalocean.com/v2/kubernetes/clusters/$(terraform output  cluster-id)/kubeconfig" > config

```

retreive dns record
```
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TF_VAR_do_token}" "https://api.digitalocean.com/v2/domains/vaquerer.com/records/"
```

delete
```
curl -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer ${TF_VAR_do_token}" "https://api.digitalocean.com/v2/domains/vaquerer.com/records/74277423"
```

create record
```
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer ${TF_VAR_do_token}" -d '{"type":"A","name":"traefik","data":"45.55.106.105","priority":null,"port":null,"ttl":1800,"weight":null,"flags":null,"tag":null}' "https://api.digitalocean.com/v2/domains/vaquerer.com/records"
```
# ansible
#k8s clusterid

```
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TF_VAR_do_token}" "https://api.digitalocean.com/v2/kubernetes/clusters" | jq -c --arg TF_VAR_do_cluster_name "${TF_VAR_do_cluster_name}" '.kubernetes_clusters[] | select(.name==$TF_VAR_do_cluster_name) | .id'
```

retrieve file kubectl DO API
```
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TF_VAR_do_token}" "https://api.digitalocean.com/v2/kubernetes/clusters/${TF_VAR_do_cluster_name}/kubeconfig" 

```
```
ansible-playbook kubectl-connection.yml
```

# helm

service account
```
kubectl --kubeconfig=config -n kube-system create serviceaccount tiller

```
serciceaccount cluster-admin role
```
kubectl  --kubeconfig=config create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
```
install helm 
```
helm --kubeconfig=config init --service-account tiller
```

demo dashboard
```
helm --kubeconfig=config  install stable/kubernetes-dashboard --name dashboard-demo
```





# mics

digitalocean volumes k8s

https://www.digitalocean.com/docs/kubernetes/how-to/add-volumes/

```
kubectl --kubeconfig=config apply -f digitalocean-storage/grafana-clain.yaml 
kubectl --kubeconfig=config get pv
```


download helm chart


debug grafana helm
```
https://www.devtech101.com/2018/10/23/deploying-helm-tiller-prometheus-alertmanager-grafana-elasticsearch-on-your-kubernetes-cluster-part-2/
```

```
helm fetch stable/grafana --untar

```

helm install chart
```
helm --kubeconfig=config upgrade  --install --set web.name="todoapp" --set web.image="nginx" --set web.host="todoapp.vaquerer.com" --wait --force todoapp ./k8s-chart
helm  upgrade  --install --set web.name="todoapp" --set web.image="nginx" --set web.host="todoapp.vaquerer.com" --wait --force todoapp ./k8s-chart
```


### helm install 

create helm.sh with this
```
https://raw.githubusercontent.com/helm/helm/master/scripts/get
```
then 
```
helm init
```
tiller account
```
helm init --service-account tiller
```

if imcopmatible version is find use
```
helm init --upgrade 
```



traefik
```
https://www.alibabacloud.com/blog/how-to-configure-trafeik-for-routing-applications-in-kubernetes_594720
```


helm digitalocean
```
https://medium.com/@stepanvrany/terraforming-dok8s-helm-and-traefik-included-7ac42b5543dc
```

gitlab-ci
```
https://medium.com/@yanick.witschi/automated-kubernetes-deployments-with-gitlab-helm-and-traefik-4e54bec47dcf
```


# grafana 

```
helm install stable/grafana \
    -f monitoring/grafana/values.yml \
    --namespace monitoring \ 
    --name grafana
```

# grafana password 

```
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```


## traefik install

create traefik chart 
```
helm install stable/traefik --name traefik --set dashboard.enabled=true,dashboard.domain=traefik.vaquerer.com,rbac.enabled=true --namespace kube-system
```


load balancer ip
```
kubectl describe svc traefik --namespace kube-system | grep Ingress | awk '{print $3}'
```

update right DNS digitalocean

jq filtering to obtain id
```
cat test.json | jq -c '.domain_records[] | select(.name=="traefik") | .id' 
```



DO monitoring
```
https://www.digitalocean.com/community/tutorials/how-to-set-up-digitalocean-kubernetes-cluster-monitoring-with-helm-and-prometheus-operator
```

ENV vars
```
https://stackoverflow.com/questions/48296082/how-to-set-dynamic-values-with-kubernetes-yaml-file
```

## stress testing

```
helm install  --name load-gen --set replicaCount=6 load-gen-master

```


sustitute domain in the go // without helm 
```
cat alertmanager-ingress.yaml | sed "s/domain/$TF_VAR_domain/g"
```
then pipe and execute
```
| kubectl apply -f - 
```